﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VSJira.Core.UI
{
    /// <summary>
    /// Interaction logic for JiraIssuesUserControl.xaml
    /// </summary>
    public partial class JiraIssuesUserControl : UserControl, IDisposable
    {
        private readonly JiraIssuesUserControlViewModel _viewModel;
        private readonly ThemeManager _themeManager;

        public JiraIssuesUserControl(JiraIssuesUserControlViewModel viewModel)
        {
            InitializeComponent();

            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            this.DataContext = this._viewModel = viewModel;
            viewModel.IssuePager.ColumnHeaderChanged += IssuePager_HeaderChanged;
            viewModel.Services.VisualStudioServices.OptionsChanged += VisualStudioServices_OptionsChanged;

            var theme = viewModel.Services.VisualStudioServices.GetConfigurationOptions().Theme;
            _themeManager = new ThemeManager(this.Resources, theme);
        }

        void IssuePager_HeaderChanged(object sender, JiraIssuesPagerViewModel.ColumnHeaderChangeEventArgs e)
        {
            var column = this.IssuesGrid.Columns.FirstOrDefault(c => c.Header.ToString() == e.Field);

            if (column != null)
            {
                column.SortDirection = e.SortDirection;
            }
        }

        private async void IssuesGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            e.Handled = true;
            await this._viewModel.IssuePager.SortIssuesAsync(e.Column.Header.ToString());
        }

        void VisualStudioServices_OptionsChanged(object sender, OptionsChangedEventArgs e)
        {
            _themeManager.Apply(e.Options.Theme);
        }

        public void Dispose()
        {
            if (_viewModel != null && _viewModel.Services != null)
            {
                _viewModel.Services.VisualStudioServices.OptionsChanged -= VisualStudioServices_OptionsChanged;
            }
        }
    }
}
