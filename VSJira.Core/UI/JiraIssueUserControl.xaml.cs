﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VSJira.Core.UI
{
    /// <summary>
    /// Interaction logic for JiraIssueUserControl.xaml
    /// </summary>
    public partial class JiraIssueUserControl : UserControl, IDisposable
    {
        private readonly ThemeManager _themeManager;
        private readonly JiraIssueUserControlViewModel _viewModel;

        public JiraIssueUserControl(JiraIssueUserControlViewModel viewModel)
        {
            InitializeComponent();

            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            this.DataContext = viewModel;

            this._themeManager = new ThemeManager(this.Resources);
            this._viewModel = viewModel;

            viewModel.Initialized += viewModel_Initialized;
        }

        void viewModel_Initialized(object sender, EventArgs e)
        {
            // set the initial theme of the control.
            var theme = _viewModel.Services.VisualStudioServices.GetConfigurationOptions().Theme;
            _themeManager.Apply(theme);

            // subscribe to changes to update the theme.
            this._viewModel.Services.VisualStudioServices.OptionsChanged += VisualStudioServices_OptionsChanged;
        }

        void VisualStudioServices_OptionsChanged(object sender, OptionsChangedEventArgs e)
        {
            _themeManager.Apply(e.Options.Theme);
        }

        public void Dispose()
        {
            if (this._viewModel != null && this._viewModel.Services != null)
            {
                this._viewModel.Services.VisualStudioServices.OptionsChanged -= VisualStudioServices_OptionsChanged;
            }
        }

        private async void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.OriginalSource is TabControl)
            {
                var tabItem = e.AddedItems.Cast<TabItem>().FirstOrDefault();

                if (tabItem != null && this._viewModel != null)
                {
                    if (tabItem.Header.ToString() == "Comments" && this._viewModel.CommentsPager == null)
                    {
                        // instantiate pager and request comments the first time the tab is selected.
                        this._viewModel.CommentsPager = new JiraCommentsPagerViewModel(this._viewModel.Issue, this._viewModel.Services);
                        await this._viewModel.CommentsPager.ResetItemsAsync();
                    }
                    else if (tabItem.Header.ToString() == "Attachments")
                    {
                        await this._viewModel.LoadAttachmentsAsync();
                    }
                }
            }
        }
    }
}
